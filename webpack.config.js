const path = require('path');
const htmlWebPackPlugin = require('html-webpack-plugin')
const {
  CleanWebpackPlugin
} = require('clean-webpack-plugin')
module.exports = {
  mode: 'development', //当前环境模式
  entry: path.resolve(__dirname, './src/index.js'), //入口文件
  output: {
    path: path.resolve(__dirname, './dist'), //打包后文件存放的地方
    filename: 'js/[name].js',
  },

  module: {
    noParse: /node_modules\/(jquey|moment|\.js)/, //module.noParse 配置哪些文件可以脱离webpack的解析
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', "@babel/preset-react"]
        }
      }
    }]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new htmlWebPackPlugin({
      title: 'react-webpack-practice',
      filename: 'index.html',
      template: 'index.html'
    })
  ],
  // devServer: {
  //   contentBase: path.resolve(__dirname, 'dist'),
  //   host: '192.168.3.6',
  //   compress: true, //服务器压缩
  //   port: 1717
  // }
}
// "dev": "webpack-dev-server --watch --inline --open --config webpack.config.js",